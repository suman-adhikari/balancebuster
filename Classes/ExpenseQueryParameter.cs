using System;

namespace BalanceBuster.Classes
{
    public class ExpenseQueryParameter
    {
        public int Page { get; set; }
        public int Size { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Search { get; set; }

    }
}