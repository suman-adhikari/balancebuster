using System.Threading.Tasks;
using BalanceBuster.Classes;
using BalanceBuster.Data;
using Microsoft.AspNetCore.Mvc;


namespace BalanceBuster.Controllers
{

    [Route("api/[controller]")]

    // Use of ApiController helps model validation (ie model.IsValid is no more required),
    // [FromBody] such attributes is not requied as well. FormBody means from http request body
    [ApiController]
    public class ExpenseController : ControllerBase
    {

        private IExpenseService _expenseService;
        public ExpenseController(IExpenseService expenseService)
        {
            _expenseService = expenseService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetExpense(int id)
        {
            var expense = await _expenseService.GetExpense(id);
            if (expense == null)
            {
                return NotFound();
            }
            return Ok(expense);
        }


        [HttpGet]
        public async Task<IActionResult> GetAllExpense([FromQuery] ExpenseQueryParameter queryParameter)
        {
            var allExpense = await _expenseService.GetAllExpenses(queryParameter);
            if (allExpense == null)
            {
                return NotFound();
            }
            return Ok(allExpense);
        }

        [HttpPost]
        // public IActionResult AddExpense([FromBody] Expense expense)
        public async Task<ActionResult> AddExpense(Expense expense)
        {
            await _expenseService.AddExpense(expense);
            // status :201 Created
            return CreatedAtAction("GetExpense", new { id = expense.Id }, expense);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateExpense(int Id, Expense expense)
        {
            if (Id != expense.Id) return BadRequest();
            string result = await _expenseService.UpdateExpense(Id, expense);
            if (result == "Not found") return NotFound();
            // for update the status is 204 : noCOntent
            return NoContent();
        }


        // for Delete the general convention is to return the item deleted
        [HttpDelete("{id}")]
        public async Task<ActionResult<Expense>> DeleteExpense(int id)
        {
            Expense _expense = await _expenseService.DeleteExpense(id);
            if (_expenseService == null) return NotFound();
            return _expense;
        }

    }
}
