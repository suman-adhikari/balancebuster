using System;
using System.Collections.Generic;

namespace BalanceBuster.Data
{
    public static class MockData
    {
        public static List<Expense> expense => expenseList;

        static List<Expense> expenseList = new List<Expense>()
            {
                new Expense(){
                    Id= 1,
                    Name= "Beer",
                    CategoryId=  1,
                    Cost=250,
                    Date = DateTime.Now,
                },
                 new Expense(){
                    Id= 2,
                    Name= "Rice",
                    CategoryId=  2,
                    Cost=1600,
                    Date = DateTime.Now,
                },
                 new Expense(){
                    Id= 3,
                    Name= "Meat",
                    CategoryId=  2,
                    Cost=170,
                    Date = DateTime.Now,
                }
            };
    }
}