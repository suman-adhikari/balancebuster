using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BalanceBuster.Classes;

namespace BalanceBuster.Data
{
    public interface IExpenseService
    {

        Task<IEnumerable<Expense>> GetAllExpenses(ExpenseQueryParameter queryParameter);
        Task<Expense> GetExpense(int id);
        Task<string> UpdateExpense(int id, Expense expense);
        Task<Expense> DeleteExpense(int id);
        Task<Expense> AddExpense(Expense expense);
    }
}