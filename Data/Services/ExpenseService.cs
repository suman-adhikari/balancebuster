using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BalanceBuster.Classes;
using Microsoft.EntityFrameworkCore;

namespace BalanceBuster.Data
{
    public class ExpenseService : IExpenseService
    {
        private readonly ExpenseContext _context;
        public ExpenseService(ExpenseContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<IEnumerable<Expense>> GetAllExpenses(ExpenseQueryParameter queryParameter)
        {
            IQueryable<Expense> expenses = _context.Expenses;

            // date filter
            if (queryParameter.StartDate != null && queryParameter.EndDate != null)
            {
                expenses = expenses.Where(e => e.Date >= queryParameter.StartDate && e.Date <= queryParameter.EndDate);
            }
            else if (queryParameter.StartDate != null)
            {
                expenses = expenses.Where(e => e.Date >= queryParameter.StartDate);
            }
            else if (queryParameter.EndDate != null)
            {
                expenses = expenses.Where(e => e.Date <= queryParameter.EndDate);
            }

            //seacrhing
            if (queryParameter.Search != null)
            {
                expenses = expenses.Where(e => e.Name.ToLower().Contains(queryParameter.Search.ToLower()));
            }

            //pagination
            expenses = expenses.Skip(queryParameter.Size * (queryParameter.Page - 1)).Take(queryParameter.Size);


            return await expenses.ToListAsync();

        }
        public async Task<Expense> GetExpense(int id) => await _context.Expenses.FirstOrDefaultAsync(x => x.Id == id);
        public async Task<Expense> AddExpense(Expense expense)
        {
            _context.Expenses.Add(expense);
            await _context.SaveChangesAsync();
            return expense;
        }

        public async Task<Expense> DeleteExpense(int id)
        {
            Expense expense = await _context.Expenses.FindAsync(id);
            if (expense == null) return expense;
            _context.Expenses.Remove(expense);
            await _context.SaveChangesAsync();
            return expense;
        }

        public async Task<string> UpdateExpense(int id, Expense expense)
        {
            _context.Entry(expense).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Expenses.Find(id) == null) return "Not found";
                throw;
            }
            return "success";

        }
    }
}