using System;
using Microsoft.EntityFrameworkCore;

namespace BalanceBuster.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Kitchen" },
                new Category { Id = 2, Name = "Clothes" },
                new Category { Id = 3, Name = "Beverage" }
            );


            modelBuilder.Entity<Expense>().HasData(
            new Expense { Id = 1, CategoryId = 1, Name = "Meat", Cost = 500, Date = DateTime.Now },
            new Expense { Id = 2, CategoryId = 2, Name = "Grunge Skater Jeans", Cost = 500, Date = DateTime.Now },
            new Expense { Id = 3, CategoryId = 3, Name = "Beer", Cost = 500, Date = DateTime.Now }
            );

        }
    }
}