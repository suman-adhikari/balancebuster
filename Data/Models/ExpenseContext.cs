using BalanceBuster.Data;
using Microsoft.EntityFrameworkCore;

public class ExpenseContext : DbContext
{
    public ExpenseContext(DbContextOptions<ExpenseContext> options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>().HasMany(c => c.Expenses).
        WithOne(c => c.Category).HasForeignKey(c => c.CategoryId);

        modelBuilder.Seed();
    }
    public DbSet<Expense> Expenses { get; set; }
    public DbSet<Category> Categories { get; set; }
}