using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BalanceBuster.Data
{
    public class Category
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public virtual List<Expense> Expenses { get; set; }

    }

}