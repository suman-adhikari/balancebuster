using System;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace BalanceBuster.Data
{
    public class Expense
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        //public int Type { get; set; }
        public int Cost { get; set; }
        public DateTime? Date { get; set; }

        //[JsonPropertyName("Type")]
        public int CategoryId { get; set; }

        [JsonIgnore]
        public virtual Category Category { get; set; }
    }

}