import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import Expenses from "./components/Expense/Expenses";
import { Counter } from "./components/Counter";
import ManageExpense from "./components/Expense/ManageExpense";
import { ToastProvider } from "react-toast-notifications";

import "./custom.css";

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <ToastProvider>
        <Layout>
          <Route exact path="/" component={Home} />
          <Route path="/counter" component={Counter} />
          <Route path="/expenses" component={Expenses} />
          <Route path="/expense/:id" component={ManageExpense} />
          <Route exact path="/expense" component={ManageExpense} />
        </Layout>
      </ToastProvider>
    );
  }
}
