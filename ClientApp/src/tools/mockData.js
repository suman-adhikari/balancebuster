const expenseType = [
  { id: 1, Name: "Kitchen" },
  { id: 2, Name: "Clothes" },
  { id: 3, Name: "Misc" },
];

const newExpense = {
  Id: null,
  Name: "",
  CategoryId: null,
  Cost: null,
  Date: null,
};

const expenses = [
  {
    Id: 1,
    Name: "Test",
    CategoryId: 1,
    Cost: 200,
    Date: "2020-05-01",
  },
  {
    Id: 2,
    Name: "Test1",
    CategoryId: 2,
    Cost: 200,
    Date: "2020-05-01",
  },
];

// Using CommonJS style export so we can consume via Node (without using Babel-node)
module.exports = {
  expenseType,
  newExpense,
  expenses,
};
