import * as expenseActions from "./expenseActions";
import { expenses } from "../../tools/mockData";

import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";

//test the async action
const middleware = [thunk];
const mockStore = configureMockStore(middleware);
const store = mockStore();
import axios from "axios";
jest.mock("axios", () => jest.fn());

// describe("Async Actions", () => {
//   afterEach(() => {
//     jest.restoreAllMocks();
//   });
// });

describe("load expense Thunk", () => {
  afterEach(() => {
    jest.resetAllMocks();
    store.clearActions(); //v imp, else store.getActions() will give previou actions as well
  });

  it("should create LOAD_EXPENSE_SUCCESS when loading expenses", () => {
    //arrange
    axios.mockResolvedValue({ data: expenses });

    //act
    const expectedActions = [{ type: "LOAD_EXPENSE_SUCCESS", expenses }];

    //assert
    return store.dispatch(expenseActions.loadExpense()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("should create CREATE_EXPENSE_SUCCESS when saving expenses", () => {
    //arrange
    const expense = { ...expenses[0], ...{ Id: null } };
    axios.mockResolvedValue({ data: expense });

    //act
    const expectedActions = [{ type: "CREATE_EXPENSE_SUCCESS", expense }];

    //assert
    return store.dispatch(expenseActions.saveExpense(expense)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("should create UPDATE_EXPENSE_SUCCESS when updating expenses", () => {
    //arrange
    const expense = expenses[0];
    axios.mockResolvedValue({ data: expense });

    //act
    const expectedActions = [{ type: "UPDATE_EXPENSE_SUCCESS", expense }];

    //assert
    return store.dispatch(expenseActions.saveExpense(expense)).then((data) => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("should create DELETE_EXPENSE_SUCCESS when deleting expenses", () => {
    //arrange
    const expense = expenses[0];
    axios.mockResolvedValue({ data: expense });

    //act
    const expectedActions = [{ type: "DELETE_EXPENSE_SUCCESS", expense }];

    //assert
    return store.dispatch(expenseActions.deleteExpense(expense)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
