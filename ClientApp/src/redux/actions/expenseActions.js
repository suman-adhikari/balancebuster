import * as types from "./actionTypes";
import * as expenseApi from "../../api/expenseApi";

export function LoadExpenseSuccess(expenses) {
  return { type: types.LOAD_EXPENSE_SUCCESS, expenses };
}

export function CreateExpenseSuccess(expense) {
  return { type: types.CREATE_EXPENSE_SUCCESS, expense };
}

export function UpdateExpenseSuccess(expense) {
  return { type: types.UPDATE_EXPENSE_SUCCESS, expense };
}

export function DeleteExpenseSuccess(expense) {
  return { type: types.DELETE_EXPENSE_SUCCESS, expense };
}

export function loadExpense(queryParameter) {
  return function (dispatch) {
    return expenseApi.loadExpense(queryParameter).then((response) => {
      // setTimeout(function () {
      dispatch(LoadExpenseSuccess(response));
      //  }, 1000);
    });
  };
}

export function GetCount(queryParameter) {
  return expenseApi.loadExpense(queryParameter);
}

export function saveExpense(expense) {
  return function (dispatch) {
    return expenseApi.saveExpense(expense).then((savedExpense) => {
      //expense is already created with axios post, the next step is just updating store. SO the
      //name is creteexpensesuccess
      expense.Id
        ? dispatch(UpdateExpenseSuccess(expense))
        : dispatch(CreateExpenseSuccess(savedExpense));
    });
  };
}

export function deleteExpense(expense) {
  return function (dispatch) {
    return expenseApi
      .deleteExpense(expense)
      .then((deletedExpense) => {
        dispatch(DeleteExpenseSuccess(deletedExpense));
      })
      .catch((error) => {
        alert(error.message);
      });
  };
}
