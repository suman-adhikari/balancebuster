import expenseReducer from "./expenseReducer";
import * as actions from "../actions/expenseActions";
import { expenses } from "../../tools/mockData";

it("should add expense when passed action CREATE_EXPENSE_SUCCESS", () => {
  //arrange
  const initialState = [];
  const newExpense = { Id: 1, Name: "Test" };
  const action = actions.CreateExpenseSuccess(newExpense);

  //act
  const newState = expenseReducer(initialState, action);

  //assert
  expect(newState.length).toEqual(1);
  expect(newState[0].Name).toEqual("Test");
});

it("should update expense when passed action UPDATE_EXPENSE_SUCCESS", () => {
  //arrange
  const initialState = [
    { Id: 1, Name: "Test1" },
    { Id: 2, Name: "Test2" },
  ];
  const updateExpense = { Id: 1, Name: "Test" };
  const action = actions.UpdateExpenseSuccess(updateExpense);

  //act
  const newState = expenseReducer(initialState, action);
  const updatedExpense = newState.find((e) => e.Id === updateExpense.Id);
  const untouchedExpense = newState.find((e) => e.Id == 2);

  //assert
  expect(newState.length).toEqual(2);
  expect(updatedExpense.Name).toEqual("Test");
  expect(untouchedExpense.Name).toEqual("Test2");
});

it("should Delete expense when passed action DELETE_EXPENSE_SUCCESS", () => {
  //arrange
  const initialState = [
    { Id: 1, Name: "Test1" },
    { Id: 2, Name: "Test2" },
  ];
  const deleteExpense = { Id: 1, Name: "Test1" };
  const action = actions.DeleteExpenseSuccess(deleteExpense);

  //act
  const newState = expenseReducer(initialState, action);
  const untouchedExpense = newState.find((e) => e.Id == 2);

  //assert
  expect(newState.length).toEqual(1);
  expect(untouchedExpense.Name).toEqual("Test2");
});

it("should load expense when passed action LOAD_EXPENSE_SUCCESS", () => {
  //arrange

  const initialState = [];

  const state = [
    { Id: 1, Name: "Test1" },
    { Id: 2, Name: "Test2" },
  ];

  const action = actions.LoadExpenseSuccess(state);

  //act
  const newState = expenseReducer(initialState, action);

  //assert
  expect(newState.length).toEqual(2);
  expect(newState[0].Name).toEqual("Test1");
});
