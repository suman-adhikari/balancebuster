import * as types from "../actions/actionTypes";
import initialState from "../reducers/initialState";

export default function expenseReducer(state = initialState.expenses, actions) {
  switch (actions.type) {
    case types.LOAD_EXPENSE_SUCCESS:
      return actions.expenses;
    case types.CREATE_EXPENSE_SUCCESS:
      return [...state, { ...actions.expense }];
    case types.UPDATE_EXPENSE_SUCCESS:
      return state.map((expense) => {
        var x = expense.Id === actions.expense.Id ? actions.expense : expense;
        return x;
      });
    case types.DELETE_EXPENSE_SUCCESS:
      return state.filter((expense) => expense.Id !== actions.expense.Id);
    default:
      return state;
  }
}
