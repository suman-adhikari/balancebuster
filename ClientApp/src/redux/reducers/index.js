import { combineReducers } from "redux";
import expenses from "../reducers/expenseReducer";

const rootReducer = combineReducers({
  expenses,
});

export default rootReducer;
