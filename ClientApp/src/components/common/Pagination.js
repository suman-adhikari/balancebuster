import React from "react";
import { Link } from "react-router-dom";

const Pagination = ({ count, size, page }) => {
  let no_of_item = Math.round(count / size);

  var items = Array(no_of_item)
    .fill()
    .map((val, index) => {
      ++index;
      let link = `/expenses?size=${size}&page=${index}`;

      let activeClass = index === page ? "page-item active" : "page-item";

      return (
        <li key={index} className={activeClass}>
          <Link className="page-link" to={link}>
            {index}
          </Link>
        </li>
      );
    });

  //for(var i=1;i<=x;i++){

  //}

  return (
    <>
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          <li className="page-item disabled">
            <Link className="page-link" to="/expenses?size=5&page=1">
              Prev
            </Link>
          </li>

          {items}

          <li className="page-item disabled">
            <Link className="page-link" to="/expenses?size=5&page=1">
              Next
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Pagination;
