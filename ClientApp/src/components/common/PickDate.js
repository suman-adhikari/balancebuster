import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const PickDate = ({ getDate }) => {
  const [startDate, setStartDate] = useState();
  return (
    <DatePicker
      selected={startDate}
      onChange={(date) => {
        setStartDate(date);
        getDate(date);
      }}
    />
  );
};

export default PickDate;
