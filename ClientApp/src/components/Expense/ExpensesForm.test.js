import React from "react";
import ExpensesForm from "./ExpensesForm";
import { shallow } from "enzyme";

// the actual save button click will be handled manageExpense.test.js

function renderExpenseForm(args) {
  const defaultProps = {
    expense: {},
    expenseType: [],
    saving: false,
    errors: {},
    onSave: () => {},
    onChange: () => {},
  };

  const props = { ...defaultProps, ...args };
  return shallow(<ExpensesForm {...props} />);
}

//describe("Expense Form Testing", () => {
it("renders form and heading text", () => {
  const wrapper = renderExpenseForm();
  expect(wrapper.find("form").length).toBe(1);
  expect(wrapper.find("h2").text()).toEqual("Save Expense");
});

it("render form heading text as 'Edit Expense' for edit ", () => {
  const wrapper = renderExpenseForm({ expense: { Id: 1 } });
  expect(wrapper.find("h2").text()).toEqual("Edit Expense");
});

it("labels save button as 'Save' when not saving", () => {
  const wrapper = renderExpenseForm();
  expect(wrapper.find("button").text()).toBe(" Save");
});

it("labels save button as 'Saving...' when  saving", () => {
  const wrapper = renderExpenseForm({ saving: true });
  expect(wrapper.find("button").text()).toBe(" Saving...");
});
//});
