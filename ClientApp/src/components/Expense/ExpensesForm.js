import React from "react";
import TextInput from "../common/TextInput";
import SelectInput from "../common/SelectInput";
import moment from "moment";

const ExpensesForm = ({
  expense,
  expenseType,
  onSave,
  onChange,
  errors,
  saving,
}) => {
  return (
    <form onSubmit={onSave}>
      <h2>{expense.Id ? "Edit" : "Save"} Expense</h2>
      <TextInput
        type="text"
        name="Name"
        label="Name"
        value={expense.Name}
        onChange={onChange}
        placeholder={errors.Name}
        error={errors.Name}
      />
      <SelectInput
        name="CategoryId"
        label="Type"
        value={expense.CategoryId || ""}
        defaultOption="--Expense Type--"
        onChange={onChange}
        options={expenseType.map((item) => ({
          value: item.id,
          text: item.Name,
        }))}
        error={errors.Type}
      />
      <TextInput
        type="number"
        name="Cost"
        label="Cost"
        value={expense.Cost ? String(expense.Cost) : ""}
        onChange={onChange}
        placeholder={errors.Cost}
        error={errors.Cost}
      />
      <TextInput
        type="date"
        name="Date"
        label="Date"
        value={moment(expense.Date).format("YYYY-MM-DD") || ""}
        onChange={onChange}
        placeholder={errors.Cost}
        error={errors.Date}
      />

      <button type="Submit" className="btn btn-primary" disabled={saving}>
        <span
          className={saving ? "spinner-border spinner-border-sm" : ""}
          role="status"
          aria-hidden="true"
        ></span>
        {saving ? " Saving..." : " Save"}
      </button>
    </form>
  );
};

export default ExpensesForm;
