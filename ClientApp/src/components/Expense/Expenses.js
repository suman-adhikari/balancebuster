import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  GetCount,
  loadExpense,
  deleteExpense,
} from "../../redux/actions/expenseActions";
import { Spinner } from "../common/spinner";
import PickDate from "../common/PickDate";
import Pagination from "../common/Pagination";
import ExpensesList from "./ExpensesList";
import { expenseType } from "../../../src/tools/mockData";
import { Redirect } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import moment from "moment";
import TextInput from "../common/TextInput";

export function Expenses({ loadExpense, expenses, deleteExpense, size, page }) {
  const [redirectToAddExpense, SetredirectToAddExpense] = useState(false);
  const [expenseCount, SetexpenseCount] = useState(0);
  const [startDate, SetstartDate] = useState(null);
  const [endDate, SetendDate] = useState(null);
  const [search, Setsearch] = useState("");

  let queryParameter = `?page=${page}&size=${size}`;
  let queryParameterCount = `?page=1&size=10000`;

  if (startDate != null) {
    queryParameter += `&startdate=${startDate}`;
    queryParameterCount += `&startdate=${startDate}`;
  }
  if (endDate != null) {
    queryParameter += `&enddate=${endDate}`;
    queryParameterCount += `&enddate=${endDate}`;
  }
  if (search.length > 1) {
    queryParameter += `&search=${search}`;
    queryParameterCount += `&search=${search}`;
  }

  const { addToast } = useToasts();

  useEffect(() => {
    //let queryParameterCount = queryParameter;
    GetCount(queryParameterCount).then((res) => {
      SetexpenseCount(res.length);
    });

    loadExpense(queryParameter);
  }, [queryParameter, loadExpense]);

  const handleDeleteExpense = (expense) => {
    if (window.confirm(`Are you Sure you want to delete: ${expense.Name} ?`)) {
      try {
        deleteExpense(expense).then(() => {
          addToast(`Expense ${expense.Name}  Deleted`, {
            appearance: "error",
            autoDismiss: true,
          });
        });
      } catch (error) {
        alert(error.message);
      }
    }
  };

  //console.log(`props: ${JSON.stringify(this.props)}`);
  //console.log(`state: ${JSON.stringify(this.state)}`);

  const getStartDate = (value) => {
    SetstartDate(moment(value).format("YYYY-MM-DD"));
  };

  const getEndDate = (value) => {
    SetendDate(moment(value).format("YYYY-MM-DD"));
  };

  const getSearch = (event) => {
    Setsearch(event.target.value);
  };

  let content =
    expenses.length === 0 ? (
      <Spinner />
    ) : (
      <>
        <div className="row" style={{ marginBottom: 5 }}>
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-6">
                <span className="lbl">
                  <label>From </label> <PickDate getDate={getStartDate} />
                </span>
              </div>
              <div className="col-md-6">
                <span className="lbl">
                  <label> To </label> <PickDate getDate={getEndDate} />
                </span>
              </div>
            </div>
          </div>

          <div className="offset-md-1 col-md-2">
            <input
              className="pad-6"
              onChange={getSearch}
              type="text"
              placeholder="search"
            />
            {/* <TextInput name="" type="text" placeholder="search" /> */}
          </div>

          {
            <div className="offset-md-1 col-md-2 float-right">
              <button
                className="btn btn-primary flatBtn"
                onClick={() => SetredirectToAddExpense(true)}
              >
                Add Expense
              </button>
            </div>
          }
        </div>
        <ExpensesList expenses={expenses} onDeleteClick={handleDeleteExpense} />
        <Pagination count={expenseCount} size={size} page={page} />
      </>
    );

  return (
    <>
      {redirectToAddExpense && <Redirect to="/expense" />}
      <>
        <h1>Expense</h1>
        {content}
      </>
    </>
  );
}

Expenses.propTypes = {
  loadExpense: PropTypes.func.isRequired,
  expenses: PropTypes.array.isRequired,
  deleteExpense: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
};

function mapStateToProps(state, ownProps) {
  var queryParams = new URLSearchParams(ownProps.location.search);
  const page = parseInt(
    queryParams.get("page") === null ? 1 : queryParams.get("page")
  );
  const size = parseInt(
    queryParams.get("size") === null ? 5 : queryParams.get("size")
  );
  return {
    expenses: state.expenses.map((expense) => {
      return {
        ...expense,
        expenseType: expenseType.find((t) => t.id === expense.CategoryId).Name,
        Date: moment(expense.Date).format("YYYY-MM-DD, hh:mm a"),
      };
    }),
    page,
    size,
  };
}

const mapDispatchToProps = {
  loadExpense,
  deleteExpense,
};

export default connect(mapStateToProps, mapDispatchToProps)(Expenses);
