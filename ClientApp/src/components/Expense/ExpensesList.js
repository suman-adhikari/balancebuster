import React from "react";
import { Link } from "react-router-dom";

const ExpensesList = ({ expenses, onDeleteClick }) => {
  return (
    <>
      <div className="table-responsive">
        <table className="table table-stripped table-bordered">
          <thead className="thead-dark">
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Type</th>
              <th>Cost</th>
              <th>Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {expenses.map((item) => (
              <tr key={item.Id}>
                <td>{item.Id}</td>
                <td>{item.Name}</td>
                <td>{item.expenseType}</td>
                <td>{item.Cost}</td>
                <td>{item.Date}</td>
                <td>
                  <Link className="btn btn-success" to={"/expense/" + item.Id}>
                    Edit
                  </Link>

                  {" | "}

                  <button
                    className="btn btn-danger"
                    onClick={() => onDeleteClick(item)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ExpensesList;
