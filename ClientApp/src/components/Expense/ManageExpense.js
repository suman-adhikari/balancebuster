import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Spinner } from "../common/spinner";
import ExpensesForm from "./ExpensesForm";
import { newExpense, expenseType } from "../../../src/tools/mockData";
import { loadExpense, saveExpense } from "../../redux/actions/expenseActions";
import { useToasts } from "react-toast-notifications";

export function ManageExpense({
  expenses,
  expenseType,
  loadExpense,
  saveExpense,
  history,
  ...props
}) {
  const [expense, setExpense] = useState({ ...props.expense });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);

  //toast
  const { addToast } = useToasts();

  //use effect is like ComponentDid Mount
  useEffect(() => {
    debugger;
    if (expenses.length === 0) {
      debugger;
      loadExpense().catch((error) => {
        alert(`cannot load expense, message: ${error.message}`);
      });
    } else {
      setExpense({ ...props.expense });
    }
  }, [props.expense, loadExpense, expenses]); // call this method only when props.expense is changed

  const handleChange = (event) => {
    const { name, value } = event.target;
    setExpense((prevExpenses) => ({
      ...prevExpenses,
      [name]:
        name === "CategoryId" || name === "Cost" ? parseInt(value, 10) : value,
    }));
  };

  function IsFormVaid() {
    const { Name, CategoryId, Cost, Date } = expense;
    const errors = {};

    if (!Name) errors.Name = "Name is required";
    if (!CategoryId) errors.Type = "Category is required";
    if (!Cost) errors.Cost = "Cost is required";
    if (!Date) errors.Date = "Name is required";

    setErrors(errors);
    return Object.keys(errors).length === 0;
  }

  const handleSave = (event) => {
    event.preventDefault();
    if (!IsFormVaid()) return;
    setSaving(true);
    saveExpense(expense)
      .then(() => {
        addToast("Expense Saved", { appearance: "success", autoDismiss: true });
        // delay to see save button state change
        //setTimeout(() => {
        history.push("/expenses");
        // }, 1000);
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  return expenseType.length === 0 || expenses.length === 0 ? (
    <Spinner />
  ) : (
    <ExpensesForm
      expense={expense}
      expenseType={expenseType}
      onSave={handleSave}
      onChange={handleChange}
      errors={errors}
      saving={saving}
    />
  );
}

ManageExpense.propTypes = {
  expenses: PropTypes.array.isRequired,
  expense: PropTypes.object.isRequired,
  expenseType: PropTypes.array.isRequired,
  saveExpense: PropTypes.func.isRequired,
  loadExpense: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export const getExpenseById = (expenses, id) =>
  expenses.find((item) => item.Id === id) || null;

function mapStateToProps(state, ownProps) {
  debugger;
  const expenses = state.expenses;
  //better use Id instead of slug : ie naming convention
  const id = parseInt(ownProps.match.params.id);
  const expense =
    id && state.expenses.length > 0
      ? getExpenseById(state.expenses, id)
      : newExpense;
  return {
    expenses,
    expense,
    expenseType,
  };
}

const mapDispatchToProps = {
  saveExpense,
  loadExpense,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageExpense);
