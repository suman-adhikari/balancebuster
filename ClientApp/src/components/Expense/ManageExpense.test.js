import React from "react";
import { mount } from "enzyme";
import ExpensesForm, { ManageExpense } from "./ManageExpense";
import { expenses, newExpense, expenseType } from "../../../src/tools/mockData";
import { loadExpense, saveExpense } from "../../redux/actions/expenseActions";
import { ToastProvider } from "react-toast-notifications";
import { createMemoryHistory } from "history";

function render(args) {
  const defaultProps = {
    expenses,
    expense: newExpense,
    expenseType,
    loadExpense: jest.fn(() => Promise.resolve()),
    // just jest().fn() is not sufficient coz saveExpense is a promise.
    // If you use just jest().fn(), you will get .then() of undefined error
    saveExpense: jest.fn(() => Promise.resolve()),
    history: { push: jest.fn() },
  };
  // if ToastProvider is not used The `useToasts` hook must be called from a descendent of the `ToastProvider`.
  const props = { ...defaultProps, ...args };
  return mount(
    <ToastProvider>
      <ManageExpense {...props} />
    </ToastProvider>
  );
}

it("set error when save as empty form is submitted", () => {
  const wrapper = render();
  wrapper.find("form").simulate("submit");
  expect(wrapper.find(".form-error").exists()).toEqual(true);
});

it("should populate form when expense model is passed", () => {
  const wrapper = render({ expense: { ...expenses[0], ...{ Id: null } } });
  expect(wrapper.find("input").first().props().value.length).toBeGreaterThan(1);
  expect(wrapper.find("input").first().props().value).toEqual("Test");
  wrapper.find("form").simulate("submit");
});

it("should call history on form submit", () => {
  const history = { push: jest.fn() };
  const wrapper = render({
    expense: { ...expenses[0], ...{ Id: null } },
    history: history,
  });
  wrapper.find("form").simulate("submit");
  return new Promise((resolve) => setImmediate(resolve)).then(() => {
    expect(history.push.mock.calls[0][0]).toBe("/expenses");
    expect(history.push).toHaveBeenCalled();
    //OR
    expect(history.push.mock.calls.length).toBe(1);
  });
});

it("should load spinner/loading when data is not loaded", () => {
  const wrapper = render({ expenses: [], expenseType: [] });
  expect(wrapper.find(".spinner-border").exists()).toEqual(true);
});

it("should not load spinner/loading when data is  present", () => {
  const wrapper = render();
  expect(wrapper.find(".spinner-border").exists()).toEqual(false);
});

it("should go to catch  when calling API fails", () => {
  debugger;
  const expenses = [];
  const loadExpense = jest.fn(() => Promise.reject());
  const saveExpense = jest.fn(() => Promise.reject());
  const wrapper = render({ loadExpense, saveExpense, expenses });
  //expect().exists()).toEqual(false);
});
