import axios from "axios";
const baseUrl = "/api/Expense";

export function loadExpense(queryParameter) {
  return axios({
    method: "Get",
    url: baseUrl + queryParameter,
  }).then((response) => response.data);
}

export function saveExpense(expense) {
  const Url = expense.Id ? baseUrl + expense.Id : baseUrl;
  return axios({
    method: expense.Id ? "put" : "post",
    url: Url,
    data: expense,
  })
    .then((response) => response.data)
    .catch((error) => {});
}

export function deleteExpense(expense) {
  return axios({ method: "delete", url: baseUrl + expense.Id }).then(
    (response) => response.data
  );
}
